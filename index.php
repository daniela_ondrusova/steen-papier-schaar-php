<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <link rel='stylesheet' href='style.css'/>

    <title>Steen, Papier, Schaar</title>
</head>

<body>
    <div class="home">
        <h1>Hoe wil je spelen?</h1>
        <a href="game.php">Speler vs Speler</a>
        <p>of</p>
        <a href="computer.php">Speler vs Computer</a>
    </div>
</body>

</html>
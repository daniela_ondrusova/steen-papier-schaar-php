<?php

session_start();

if (!isset($_SESSION['keuzes'])) {
    $_SESSION['keuzes'] = ['player1' => '', 'player2' => ''];
}

function spelers($number)
{
    echo "
    <p>Speler $number maakt keuze</p>
    <div class='choices'>
        <a href='?player$number=$number&choice$number=steen' class='choice-button'>steen</a>
        <a href='?player$number=$number&choice$number=papier' class='choice-button'>papier</a>
        <a href='?player$number=$number&choice$number=schaar' class='choice-button'>schaar</a>
    </div>";
}

function computerKeuze()
{
    $keuzes = ['steen', 'papier', 'schaar'];
    return $keuzes[array_rand($keuzes)];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='style.css' />
    <title>Steen, Papier, Schaar</title>
</head>
<body>
    <h1>Steen, Papier, Schaar</h1>

    <?php
    spelers(1);

    if (isset($_GET['player1']) && isset($_GET['choice1'])) {
        $_SESSION['keuzes']['player1'] = $_GET['choice1'];
        $_SESSION['keuzes']['player2'] = computerKeuze();

        echo "<h2>Speler 1 koos: {$_SESSION['keuzes']['player1']}</h2>";
        echo "<h2>Computer koos: {$_SESSION['keuzes']['player2']}</h2>";

        $keuze1 = $_SESSION['keuzes']['player1'];
        $keuze2 = $_SESSION['keuzes']['player2'];
        
        if ($keuze1 === $keuze2) {
            echo "<h2>Gelijkspel</h2>";
        } else if ($keuze1 === 'steen' && $keuze2 === 'papier') {
            echo "<h2>Computer wint</h2>";
        } else if ($keuze2 === 'steen' && $keuze1 === 'papier') {
            echo "<h2>Speler 1 wint</h2>";
        } else if ($keuze1 === 'steen' && $keuze2 === 'schaar') {
            echo "<h2>Speler 1 wint</h2>";
        } else if ($keuze2 === 'steen' && $keuze1 === 'schaar') {
            echo "<h2>Computer wint</h2>";
        } else if ($keuze1 === 'papier' && $keuze2 === 'schaar') {
            echo "<h2>Computer wint</h2>";
        } else if ($keuze2 === 'papier' && $keuze1 === 'schaar') {
            echo "<h2>Speler 1 wint</h2>";
        }

        echo "<a href='index.php'>Terug</a>";
    }
    ?>
</body>
</html>
